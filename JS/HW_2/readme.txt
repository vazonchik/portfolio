�������


����������� ��������� �� Javascript, ������� ����� �������� ��� ������� ����� � �������� ���������.



����������� ����������:

-
������� � ������� ���������� ���� �������� �����, ������� ������ ������������.

-������� � ������� ��� ������� ����� �� 1 �� ���������� ������������� �����.

-����������� ���������� ������������ ��������� ES6 (ES2015) ��� �������� ����������.




�� ������������ ������� ����������� ���������:


-���������, ��� ��������� �������� �������� ����� ������, � ������ �������. ���� ������ ������� �� �����������, ��������� ����� ���� �� ����� �� ��� ���, ���� �� ����� ������� ����� ����� ������ 1.

-����������� �������������� ��������, ����� �� ���������� ��� ����� ������� ��� ������� �����.

-������� ��� �����, m � n. ������� � ������� ��� ������� ����� � ��������� �� m �� n (������� �� ��������� ����� ����� m, ������� ����� n). ���� ���� �� ���� �� ����� �� ��������� ������� ���������, ��������� ����, ������� ��������� �� ������, � �������� ��� ����� ������.