let n1 = prompt("Введите первое число");

function numberValide1 (num) {
	while(isNaN(num) || num <= 1 || num%1!==0){
		num = prompt("Введите первое число", num);
	}
	return num;
}
n1 = numberValide1(n1);

let n2 = prompt("Введите второе число");
function numberValide2 (num) {
	while(isNaN(num) || num <= 1 || num%1!==0){
		num = prompt("Введите второе число", num);
	}
	return num;
}
n2 = numberValide2(n2);

let m;
let n;
if(n1>n2){
	m = n2;
	n = n1;
} else {
	m = n1;
	n = n2;
}

for (var i=m; i<=n; i++){
    for(var j=m; j<=i; j++){
        if (i%j == 0) break;
    }
    if(j==i) console.log(i);
}