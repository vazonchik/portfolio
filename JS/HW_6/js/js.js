function createUser() {
    let user = {
        getLogin() {
            return this.firstName.charAt(0).toLocaleLowerCase() + this.lastName.toLocaleLowerCase();
        }
	}
	
	Object.defineProperties (user,
		{ 
			"firstName":
			{
				value: prompt ("Введите Ваше имя", "Чюрчхела"),
				configurable: true
			},
			"setFirstName":
			{
				get(){
					return this.firstName;
				},
				set(value){
					Object.defineProperty(this, "firstName", {value});
				},
				enumerable: true
			},
		}	
	);
	
	Object.defineProperties (user,
		{ 
			"lastName":
			{
				value: prompt ("Введите Вашу фамилию", "Рябушкин"),
				configurable: true
			},
			"setLastName":
			{
				get(){
					return this.lastName;
				},
				set(value){
					Object.defineProperty(this, "lastName", {value});
				},
				enumerable: true
			},
		}	
	);

	return user;
}

console.log(createUser());