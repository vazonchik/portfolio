function copy(o) {
   let out, v, key;
   out = Array.isArray(o) ? [] : {};
   for (key in o) {
       v = o[key];
       out[key] = (typeof v === "object" && v !== null) ? copy(v) : v;
   }
   return out;
}

//let test = {
//  age: 77,
//  gender: 'Male',
//  name: {
//    first: 'Pavel',
//    second: 'Durov',
//    father: 'Valerievich',
//	  hug: {hghg: 555}
//  }
//};
//
//console.log (test);
//let yy = copy(test)
//test.name.hug.hghg = 888;
//console.log(yy);
