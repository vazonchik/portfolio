let change = document.getElementById("changeThem");
let bg = document.querySelector("body");

if (localStorage.getItem("setBg") != NaN){
	bg.style.background = localStorage.getItem("setBg");
}

change.addEventListener("click", function(){
	if (localStorage.getItem("setBg") == ""){
		localStorage.setItem("setBg", "green");
		
	} else localStorage.setItem("setBg", "");
	
	let color = localStorage.getItem("setBg");
	bg.style.background = color;
})